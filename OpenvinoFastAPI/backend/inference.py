import io
from PIL import Image

import cv2
import numpy as np
from openvino.inference_engine import IECore

from ssd import *
from utils import ColorPalette

MODELNAME = 'model/persondet/person-detection-0202.xml'
MODELBINNAME = 'model/persondet/person-detection-0202.bin'

prob_threshold = 0.3

ie = IECore()

def draw_detections(frame, detections, palette, labels, threshold, draw_landmarks=False):
    size = frame.shape[:2]
    for detection in detections:
        if detection.score > threshold:
            xmin = max(int(detection.xmin), 0)
            ymin = max(int(detection.ymin), 0)
            xmax = min(int(detection.xmax), size[1])
            ymax = min(int(detection.ymax), size[0])
            class_id = int(detection.id)
            color = palette[class_id]
            det_label = labels[class_id] if labels and len(labels) >= class_id else '#{}'.format(class_id)
            cv2.rectangle(frame, (xmin, ymin), (xmax, ymax), color, 2)
            cv2.putText(frame, '{} {:.1%}'.format(det_label, detection.score),
                        (xmin, ymin - 7), cv2.FONT_HERSHEY_COMPLEX, 0.6, color, 1)
            if draw_landmarks:
                for landmark in detection.landmarks:
                    cv2.circle(frame, landmark, 2, (0, 255, 255), 2)
    return frame

def get_model():
    model = SSD(ie, MODELNAME, labels=None, keep_aspect_ratio_resize=False)
    palette = ColorPalette(len(model.labels) if model.labels else 100)
    return model, palette


def get_infer_image(model, palette, file):
    exec_net = ie.load_network(network=model.net, device_name='CPU',  num_requests=2)
    input_image = Image.open(io.BytesIO(file)).convert("RGB")
    img = np.asarray(input_image)
    inputs, preprocessing_meta = model.preprocess(img)
    out = exec_net.infer(inputs)
    results = model.postprocess(out, preprocessing_meta)
    if results:
        objects = results
    frame = draw_detections(img, objects, palette, model.labels, prob_threshold, False)
    # cv2.imwrite('test1.jpg',frame)
    r = Image.fromarray(frame)
    return r

if __name__ == "__main__":
    model, palette = get_model()
    img = cv2.imread('test.png')
    get_infer_image(model, palette, img)

