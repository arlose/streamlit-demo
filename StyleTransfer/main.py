import cv2
import numpy as np
import streamlit as st

import config
import inference

st.set_page_config(  # Alternate names: setup_page, page, layout
	layout="wide",  # Can be "centered" or "wide". In the future also "dashboard", etc.
	initial_sidebar_state="auto",  # Can be "auto", "expanded", "collapsed"
	page_title=None,  # String or None. Strings get appended with "• Streamlit". 
	page_icon=None,  # String, anything supported by st.image, or None.
)

st.header("Style Transfer Demo")

WEBCAMNUM = 0

def styletrans(model):
    while(True):
        video_capture = cv2.VideoCapture(WEBCAMNUM)
        ret, frame = video_capture.read()
        if(frame is not None):
            frame = cv2.flip(frame,1)
            output, resized = inference.inference(model, frame)
            ORG_WINDOW.image(resized[:,:,::-1])
            FRAME_WINDOW.image(output)

if __name__ == "__main__":
    style = st.selectbox("Choose the style", [i for i in config.STYLES.keys()])
    if st.button("Style Transfer"):
        modelname = config.STYLES[style]
        model_name = f"{config.MODEL_PATH}{modelname}.t7"
        model = cv2.dnn.readNetFromTorch(model_name)
        col1,col2 = st.beta_columns(2) 
        col1.header("Original")
        ORG_WINDOW = col1.image([])
        col2.header("NewStyle")
        FRAME_WINDOW = col2.image([])
        styletrans(model)
            
