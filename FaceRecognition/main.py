import cv2
import numpy as np
import streamlit as st

import face_recognition
import pandas as pd

if __name__ == "__main__":
    st.header("Face Recognition Demo")
    while(True):
        known_face_names, known_face_encodings = load_known_data()
        video_capture = cv2.VideoCapture(WEBCAMNUM)
        frame = capture_face(video_capture)
        name, similarity, frame = recognize_frame(frame)
        FRAME_WINDOW.image(frame)
        if similarity > 0.75:
            label = f"**{name}**: *{similarity:.2%} likely*"
            st.markdown(label)
            break
    # press to restart the scripts
    st.button('contunue......')