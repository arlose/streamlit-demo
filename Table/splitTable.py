import cv2
import numpy as np

def sTable(img):
    im = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    h, w = im.shape
    dst = cv2.adaptiveThreshold(~im, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 15, -2)
    horizontal = dst.copy()
    vertical = dst.copy()
    scale = 32

    horizontalsize = horizontal.shape[1] // scale
    horizontalStructure = cv2.getStructuringElement(cv2.MORPH_RECT, (horizontalsize, 1))
    horizontal = cv2.erode(horizontal, horizontalStructure, (-1, -1))
    horizontal = cv2.dilate(horizontal, horizontalStructure, (-1, -1))

    verticalsize = vertical.shape[0] // scale
    verticalStructure = cv2.getStructuringElement(cv2.MORPH_RECT, (1, verticalsize))
    vertical = cv2.erode(vertical, verticalStructure, (-1, -1))
    vertical = cv2.dilate(vertical, verticalStructure, (-1, -1))

    table = horizontal + vertical

    morsize = 10

    tableStructure = cv2.getStructuringElement(cv2.MORPH_RECT, (morsize, morsize))
    #table = cv2.erode(table, tableStructure, (-1, -1))
    table = cv2.dilate(table, tableStructure, (-1, -1))

    ret, thresh = cv2.threshold(table, 230, 255, cv2.THRESH_BINARY)
        
    contours = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE) 

    cnts = contours[0]

    temp = np.ones(img.shape,np.uint8)*255
    maxcount = len(cnts)
    whth = 20
    count = 0

    blocks = []

    for cnt in cnts: 
        peri = cv2.arcLength(cnt, True) 
        approx = cv2.approxPolyDP(cnt, 0.03 * peri, True) 
        if len(approx) == 4 :  # and ratio(approx)
            tw = abs(approx[2][0][0]-approx[0][0][0])
            th = abs(approx[2][0][1]-approx[0][0][1])
            if (tw>10) and (th>10) and tw*th<w*h*0.9: # del line and the outest rect
                cv2.drawContours(temp, [approx], -1, (0,255,0), 1)
                count = count+1
                top = min(min(approx[0][0][1], approx[1][0][1]),min(approx[2][0][1], approx[3][0][1]))-8
                left = min(min(approx[0][0][0], approx[1][0][0]), min(approx[2][0][0], approx[3][0][0]))
                bottom = max(max(approx[0][0][1], approx[1][0][1]),max(approx[2][0][1], approx[3][0][1]))+8
                right =  max(max(approx[0][0][0], approx[1][0][0]), max(approx[2][0][0], approx[3][0][0]))
                blocks.append([top, left, bottom, right])
                # tmpimg = img[top:bottom, left:right]
                # text = pytesseract.image_to_string(tmpimg, lang='chi_sim')
                # temp = cv2ImgAddText(temp, text, left+4, top+4, (255, 0 , 0), 20)
            else:
                print('-----------------', len(approx))
                print(approx)
        elif len(approx) == 2:
            lenapprox = cv2.approxPolyDP(cnt, 0.005 * peri, True) 
            if len(lenapprox) == 4 :
                tw = abs(lenapprox[2][0][0]-lenapprox[0][0][0])
                th = abs(lenapprox[2][0][1]-lenapprox[0][0][1])
                if (tw>whth) and (th>whth) and tw*th<w*h*0.9:
                    cv2.drawContours(temp, [lenapprox], -1, (0,255,0), 1)
                    count = count+1
                    top = max(min(min(lenapprox[0][0][1], lenapprox[1][0][1]),min(lenapprox[2][0][1], lenapprox[3][0][1]))-8, 0)
                    left = max(min(min(lenapprox[0][0][0], lenapprox[1][0][0]), min(lenapprox[2][0][0], lenapprox[3][0][0])), 0)
                    bottom = min(max(max(lenapprox[0][0][1], lenapprox[1][0][1]),max(lenapprox[2][0][1], lenapprox[3][0][1]))+8, h)
                    right =  min(max(max(lenapprox[0][0][0], lenapprox[1][0][0]), max(lenapprox[2][0][0], lenapprox[3][0][0])), w)
                    blocks.append([top, left, bottom, right])
                    # tmpimg = img[top:bottom, left:right]
                    # text = pytesseract.image_to_string(tmpimg, lang='chi_sim')
                    # temp = cv2ImgAddText(temp, text, left+4, top+4, (255, 0 , 0), 20)
                else:
                    print('================', len(lenapprox))
                    print(lenapprox)
        else:
            print('$$$$$$$$$$$$$$$$$$$$$')
            print(approx)
            cv2.drawContours(temp, [approx], -1, (255,255,0), 1)
    return temp, blocks