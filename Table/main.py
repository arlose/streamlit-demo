import cv2
import numpy as np
import streamlit as st
from PIL import Image
from findTable import fTable
from splitTable import sTable
import pytesseract

from PIL import Image, ImageDraw, ImageFont

def cv2ImgAddText(img, text, left, top, textColor=(0, 255, 0), textSize=20):
    if (isinstance(img, np.ndarray)):  # 判断是否OpenCV图片类型
        img = Image.fromarray(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
    # 创建一个可以在给定图像上绘图的对象
    draw = ImageDraw.Draw(img)
    fontStyle = ImageFont.truetype("simsun.ttc", textSize, encoding="utf-8")
    # 绘制文本
    draw.text((left, top), text, textColor, font=fontStyle)
    # 转换回OpenCV格式
    return cv2.cvtColor(np.asarray(img), cv2.COLOR_RGB2BGR)

st.set_page_config(  # Alternate names: setup_page, page, layout
	layout="wide",  # Can be "centered" or "wide". In the future also "dashboard", etc.
	initial_sidebar_state="auto",  # Can be "auto", "expanded", "collapsed"
	page_title=None,  # String or None. Strings get appended with "• Streamlit". 
	page_icon=None,  # String, anything supported by st.image, or None.
)

st.header("Table Recognition Demo")

if __name__ == "__main__":
    input_image = st.file_uploader("Insert an Image")  # image upload widget
    if input_image:
        original_image = Image.open(input_image).convert("RGB")
        st.image(original_image, use_column_width=True)
        if st.button("Get table recognition result"):
            img = cv2.cvtColor(np.asarray(original_image),cv2.COLOR_RGB2BGR) 
            ft = fTable(img, 64)
            table_img = ft.process()
            if table_img is not None:
                st.image(table_img[:,:,::-1], use_column_width=True)
                frame, blocks = sTable(table_img)
                FRAME_WINDOW = st.image([])
                blocks.reverse()
                for b in blocks:
                    b[0] = max(b[0], 0)
                    b[1] = max(b[1], 0)
                    b[2] = min(b[2], frame.shape[0])
                    b[3] = min(b[3], frame.shape[1])
                    tmpimg = table_img[b[0]:b[2], b[1]:b[3]]
                    text = pytesseract.image_to_string(tmpimg, lang='chi_sim')
                    frame = cv2ImgAddText(frame, text.strip().replace('\n\n', '\n'), b[1]+8, b[0]+8, (255, 0 , 0), 28)
                    FRAME_WINDOW.image(frame[:,:,::-1])
            else:
                st.write("Can't find a table!")
    else:
        # handle case with no image
        st.write("Insert an Image")

            
