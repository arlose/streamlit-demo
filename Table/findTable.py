import cv2
import numpy as np
import imutils
import math

def draw_angled_rec(x0, y0, width, height, angle, img):
    _angle = angle * math.pi / 180.0
    b = math.cos(_angle) * 0.5
    a = math.sin(_angle) * 0.5
    pt0 = (int(x0 - a * height - b * width),
           int(y0 + b * height - a * width))
    pt1 = (int(x0 + a * height - b * width),
           int(y0 - b * height - a * width))
    pt2 = (int(2 * x0 - pt0[0]), int(2 * y0 - pt0[1]))
    pt3 = (int(2 * x0 - pt1[0]), int(2 * y0 - pt1[1]))

    cv2.line(img, pt0, pt1, (255, 255, 255), 1)
    cv2.line(img, pt1, pt2, (255, 255, 255), 1)
    cv2.line(img, pt2, pt3, (255, 255, 255), 1)
    cv2.line(img, pt3, pt0, (255, 255, 255), 1)

    return pt0

def tltrbrbl(points):
    pad = 3

    addmax = 0
    addmin = 999999
    submax = -999999
    submin = 999999

    tl = [[0,0]]
    tr = [[0,0]]
    br = [[0,0]]
    bl = [[0,0]]

    for p in points:
        addv = p[0][0]+p[0][1]
        subv = p[0][0]-p[0][1]
        if addv > addmax:
            addmax = addv
            br = p
        if addv < addmin:
            addmin = addv
            tl = p
        if subv > submax:
            submax = subv
            tr = p
        if subv < submin:
            submin = subv
            bl = p

    tl[0][0] = tl[0][0] - pad
    tl[0][1] = tl[0][1] - pad

    tr[0][0] = tr[0][0] + pad
    tr[0][1] = tr[0][1] - pad

    br[0][0] = br[0][0] + pad
    br[0][1] = br[0][1] + pad

    bl[0][0] = bl[0][0] - pad
    bl[0][1] = bl[0][1] + pad

    respoints = [tl,tr,br,bl]
    tw = int(max(math.sqrt((tl[0][0]-tr[0][0])*(tl[0][0]-tr[0][0])+(tl[0][1]-tr[0][1])*(tl[0][1]-tr[0][1])), 
                 math.sqrt((bl[0][0]-br[0][0])*(bl[0][0]-br[0][0])+(bl[0][1]-br[0][1])*(bl[0][1]-br[0][1]))))
    th = int(max(math.sqrt((tl[0][0]-bl[0][0])*(tl[0][0]-bl[0][0])+(tl[0][1]-bl[0][1])*(tl[0][1]-bl[0][1])), 
                 math.sqrt((tr[0][0]-br[0][0])*(tr[0][0]-br[0][0])+(tr[0][1]-br[0][1])*(tr[0][1]-br[0][1]))))

    print(respoints)
    print(tw, th)

    return np.float32(respoints), tw, th

class fTable():
    def __init__(self, img, scale):
        super().__init__()
        self.img = img
        self.scale = scale
        return

    def process(self):
        img = self.img.copy()
        
        im = cv2.cvtColor(self.img, cv2.COLOR_RGB2GRAY)
        h, w = im.shape
        dst = cv2.adaptiveThreshold(~im, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 15, -2)
        horizontal = dst.copy()
        vertical = dst.copy()
        scale = self.scale

        horizontalsize = horizontal.shape[1] // scale
        horizontalStructure = cv2.getStructuringElement(cv2.MORPH_RECT, (horizontalsize, 1))
        horizontal = cv2.erode(horizontal, horizontalStructure, (-1, -1))
        horizontal = cv2.dilate(horizontal, horizontalStructure, (-1, -1))

        verticalsize = vertical.shape[0] // scale
        verticalStructure = cv2.getStructuringElement(cv2.MORPH_RECT, (1, verticalsize))
        vertical = cv2.erode(vertical, verticalStructure, (-1, -1))
        vertical = cv2.dilate(vertical, verticalStructure, (-1, -1))

        table = horizontal + vertical

        # morphStructure = cv2.getStructuringElement(cv2.MORPH_RECT, (horizontalsize>>2, verticalsize>>2))
        # table = cv2.dilate(table, morphStructure, (-1, -1))

        # save > 1/20 whole page 's table
        ret, thresh = cv2.threshold(table, 230, 255, cv2.THRESH_BINARY)
        
        contours = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE) 

        cnts = contours[0] # if imutils.is_cv2() else contours[1]

        qcontours = []

        out = None

        for cnt in cnts:   
            # print(cnt)     
            # 最小外接矩形框，有方向角
            rect = cv2.minAreaRect(cnt)
            zimg = np.zeros(img.shape, dtype=np.uint8)
            if(rect[1][0]*rect[1][1]>w*h/20):
                # print(rect)
                # box = cv2.boxPoints(rect) # cv2.cv.Boxpoints() # if imutils.is_cv2() else cv2.boxPoints(rect)
                # box = np.int0(box)
                # cv2.drawContours(img, [box], 0, (0, 0, 255), 2)
                # draw_angled_rec(rect[0][0], rect[0][1], rect[1][0], rect[1][1], rect[2], img)

                hull = cv2.convexHull(cnt)
                # print(hull)
                cv2.drawContours(zimg, [hull], 0, (255, 255, 255), 1)
                zim = cv2.cvtColor(zimg, cv2.COLOR_RGB2GRAY)
                zcontours = cv2.findContours(zim, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

                zcnts = zcontours[0]

                for zcnt in zcnts: 
                    peri = cv2.arcLength(zcnt, True) 
                    approx = cv2.approxPolyDP(zcnt, 0.02 * peri, True) 
                    # print(approx)
                    if len(approx) == 4 :  # and ratio(approx)
                        qcontours.append(approx)
                        cv2.drawContours(img, [approx], -1, (0,255,0), 2)
                        pts1, tw, th = tltrbrbl(approx)
                        # 变换后分别在左上、右上、左下、右下四个点
                        pts2 = np.float32([[0, 0], [tw, 0], [tw, th], [0, th]])
                        # 生成透视变换矩阵
                        M = cv2.getPerspectiveTransform(pts1, pts2)
                        # 进行透视变换
                        out = cv2.warpPerspective(img, M, (tw, th))

        # cv2.imwrite("./temp/table1.jpg", table)
        
        return out


if __name__ == '__main__':
    img = cv2.imread('images/t2.jpg')
    ft = fTable(img, 64)
    table = ft.process()
    print(table[0].reshape(4,2))
    cv2.drawContours(img, table, -1, (0,255,0), 2)
    cv2.imwrite("./temp/table.jpg", img)
    h, w = img.shape[0:2]
    img1 = np.zeros((h, w, 3), np.uint8)
    # triangle = np.array([[0, 0], [1500, 800], [500, 400]])
    # print(triangle)
    # cv2.fillConvexPoly(img1, triangle, (255, 255, 255))
    cv2.fillConvexPoly(img1, table[0].reshape(4,2), (255, 255, 255))
    cv2.imwrite("./temp/tablemask.jpg", img1)
