import io

from fastapi import FastAPI, File
from starlette.responses import Response

from inference import get_model, get_infer_image

model = get_model()

app = FastAPI(
    title="pytorch model inference",
    description=""" """,
    version="0.1.0",
)

@app.post("/infer")
def get_model_result(file: bytes = File(...)):
    """Get model result from image file"""
    result_image = get_infer_image(model, file)
    bytes_io = io.BytesIO()
    result_image.save(bytes_io, format="PNG")
    return Response(bytes_io.getvalue(), media_type="image/png")