import config
import cv2
import numpy as np
from PIL import Image
import altair as alt

import pandas as pd

import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn.functional as F

preprocess = transforms.Compose([
    transforms.Resize(256),
    transforms.CenterCrop(224),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
])


def to_predictions_chart(predictions) -> alt.Chart:
    """A pretty chart of the (prediction, probability) to output to the user"""
    dataframe = pd.DataFrame(predictions, columns=["id", "prediction", "probability"])
    dataframe["probability"] = dataframe["probability"].round(2) * 100
    chart = (
        alt.Chart(dataframe)
        .mark_bar()
        .encode(
            x=alt.X("probability:Q", scale=alt.Scale(domain=(0, 100))),
            y=alt.Y(
                "prediction:N",
                sort=alt.EncodingSortField(field="probability", order="descending"),
            ),
        )
    )
    return chart

def inference(model, labels, image):
    img = Image.fromarray(cv2.cvtColor(image,cv2.COLOR_BGR2RGB))
    input_tensor = preprocess(img)
    input_batch = input_tensor.unsqueeze(0) 

    with torch.no_grad():
        res = model(input_batch)

    prob = F.softmax(res[0], dim=0)
    indexs = torch.argsort(-prob)

    topk = 10
    predictions = []
    for i in range(topk):
        # print(labels[indexs[i]].strip().split(',')[-1], " prob: ", prob[indexs[i]].item())
        predictions.append([indexs[i], labels[indexs[i]].strip().split(',')[-1], prob[indexs[i]].item()])

    output = labels[indexs[i]].strip().split(',')[-1]
    chart = to_predictions_chart(predictions)

    return output, chart