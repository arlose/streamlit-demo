MODEL_PATH = "./models/"

MODELNAMES = {
    "mobilenetv2": "mobilenetv2",
    "resnet18": "resnet18",
    "resnet50": "resnet50",
}