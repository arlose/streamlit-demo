import cv2
import numpy as np
import streamlit as st

import config
import inference
import torch
import torchvision
import torchvision.models as models

st.set_page_config(  # Alternate names: setup_page, page, layout
	layout="wide",  # Can be "centered" or "wide". In the future also "dashboard", etc.
	initial_sidebar_state="auto",  # Can be "auto", "expanded", "collapsed"
	page_title=None,  # String or None. Strings get appended with "• Streamlit". 
	page_icon=None,  # String, anything supported by st.image, or None.
)

st.header("Image Recognition Demo")

WEBCAMNUM = 0

def run(model, labels):
    while(True):
        video_capture = cv2.VideoCapture(WEBCAMNUM)
        ret, frame = video_capture.read()
        if(frame is not None):
            frame = cv2.flip(frame,1)
            output, predictions_chart = inference.inference(model, labels, frame)
            ORG_WINDOW.image(frame[:,:,::-1])
            CHART_WINDOW.altair_chart(predictions_chart)

if __name__ == "__main__":
    with open('chinese1000.txt', 'r', encoding='UTF-8') as clf:
        labels = clf.readlines()
    mname = st.selectbox("Choose the model", [i for i in config.MODELNAMES.keys()])
    if st.button("Run Model"):
        modelname = config.MODELNAMES[mname]
        # TODO select model
        if modelname=='mobilenetv2':
            model = models.mobilenet_v2(pretrained=True)
        if modelname=='resnet18':
            model = models.resnet18(pretrained=True)
        if modelname=='resnet50':
            model = models.resnet50(pretrained=True)
        model.eval()
        col1,col2 = st.beta_columns(2) 
        ORG_WINDOW = col1.empty()
        CHART_WINDOW = col2.empty()
        run(model, labels)

            
