import cv2
import numpy as np
import streamlit as st

import config

st.set_page_config(  # Alternate names: setup_page, page, layout
	layout="wide",  # Can be "centered" or "wide". In the future also "dashboard", etc.
	initial_sidebar_state="auto",  # Can be "auto", "expanded", "collapsed"
	page_title=None,  # String or None. Strings get appended with "• Streamlit". 
	page_icon=None,  # String, anything supported by st.image, or None.
)

st.header("Capture Training Testing Demo")

WEBCAMNUM = 0


if __name__ == "__main__":
    style = st.selectbox("Choose the process", [i for i in config.PROCESS.keys()])
    if st.button(""):
        modelname = config.STYLES[style]
        model_name = f"{config.MODEL_PATH}{modelname}.t7"
        model = cv2.dnn.readNetFromTorch(model_name)
        col1,col2 = st.beta_columns(2) 
        col1.header("Original")
        ORG_WINDOW = col1.image([])
        col2.header("NewStyle")
        FRAME_WINDOW = col2.image([])
        styletrans(model)
            
