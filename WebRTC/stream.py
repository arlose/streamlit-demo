import cv2
import streamlit as st

from aiortc.contrib.media import MediaPlayer

from streamlit_webrtc import (
    ClientSettings,
    VideoTransformerBase,
    WebRtcMode,
    webrtc_streamer,
)

WEBRTC_CLIENT_SETTINGS = ClientSettings(
    media_stream_constraints={"video": True, "audio": True},
)

media_file_info = 'http://192.168.0.142:8080/video'

def create_player():
    return MediaPlayer(media_file_info)

webrtc_streamer(
        key=f"media-streaming",
        mode=WebRtcMode.RECVONLY,
        client_settings=WEBRTC_CLIENT_SETTINGS,
        player_factory=create_player,
    )