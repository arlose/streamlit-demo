import cv2
import queue
import streamlit as st

from streamlit_webrtc import (
    ClientSettings,
    VideoTransformerBase,
    WebRtcMode,
    webrtc_streamer,
)

WEBRTC_CLIENT_SETTINGS = ClientSettings(
    media_stream_constraints={"video": True, "audio": False},
)

webrtc_ctx = webrtc_streamer(
        key="loopback",
        mode=WebRtcMode.SENDONLY,
        client_settings=WEBRTC_CLIENT_SETTINGS,
    )

if webrtc_ctx.video_receiver:
    image_loc = st.empty()
    while True:
        try:
            frame = webrtc_ctx.video_receiver.get_frame(timeout=1)
        except queue.Empty:
            print("Queue is empty. Stop the loop.")
            webrtc_ctx.video_receiver.stop()
            break

        img_rgb = frame.to_ndarray(format="rgb24")
        image_loc.image(img_rgb)