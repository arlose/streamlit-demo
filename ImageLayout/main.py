import streamlit as st
import os
import time

import numpy as np
import imutils
import cv2

import base64

st.set_page_config(  # Alternate names: setup_page, page, layout
	layout="wide",  # Can be "centered" or "wide". In the future also "dashboard", etc.
	initial_sidebar_state="auto",  # Can be "auto", "expanded", "collapsed"
	page_title=None,  # String or None. Strings get appended with "• Streamlit". 
	page_icon=None,  # String, anything supported by st.image, or None.
)

CLMNSNUM = 4
IMGWIDTH = 200

imgs = []

img = cv2.imread('../data/hand.jpg')

largewin = st.image(img[:,:,::-1])

cols1 = st.beta_columns(CLMNSNUM) 

# for i in range(CLMNSNUM):
#     cols1[i] = st.image([])

for i in range(CLMNSNUM*2):
    imgname = '../data/smallimg/%04d.png'%(i+1)
    imgs.append(cv2.imread(imgname))

for i in range(CLMNSNUM):
    cols1[i].image(imgs[i+CLMNSNUM][:,:,::-1])

for i in range(CLMNSNUM):
    cols1[i].empty()
    cols1[i].image(imgs[i+CLMNSNUM][:,:,::-1])


image_urls = ['../data/smallimg/0001.png', '../data/smallimg/0002.png', 
'../data/smallimg/0003.png', '../data/smallimg/0004.png', 
'../data/smallimg/0005.png', '../data/smallimg/0006.png', 
'../data/smallimg/0007.png', '../data/smallimg/0008.png']

opening_html = '<div style=display:flex;flex-wrap:wrap>'
closing_html = '</div>'
# child_html = ['<img src="{}" style=margin:3px;width:200px;></img>'.format(url) for url in image_urls]
# child_html = '<img src="file:///Users/fengjie/Work/Demo/streamlit-demo/data/hand.jpg" style=margin:3px;width:200px;></img>'
child_html = ''

for imgurl in image_urls:
    file_ = open(imgurl, "rb")
    contents = file_.read()
    data_url = base64.b64encode(contents).decode("utf-8")
    file_.close()
    child_html += f'<img src="data:image/jpg;base64,{data_url}" alt="test" style=margin:3px;width:100px;></img>\n'

gallery_html = opening_html
gallery_html += child_html
gallery_html += closing_html

labels_placeholder = st.empty()
# st.markdown(gallery_html)
labels_placeholder.markdown(gallery_html, unsafe_allow_html=True)
# for i in range(CLMNSNUM):
#     imgname = '../data/smallimg/%04d.png'%(i+1+CLMNSNUM)
#     imgs.append(cv2.imread(imgname))
#     cols1[i].empty()
#     cols1[i].image(imgs[i+CLMNSNUM][:,:,::-1], width = IMGWIDTH)

time.sleep(10)

image_urls = ['../data/smallimg/0011.png', '../data/smallimg/0012.png', 
'../data/smallimg/0013.png', '../data/smallimg/0014.png', 
'../data/smallimg/0015.png', '../data/smallimg/0016.png', 
'../data/smallimg/0017.png', '../data/smallimg/0018.png']

child_html = ''

for imgurl in image_urls:
    file_ = open(imgurl, "rb")
    contents = file_.read()
    data_url = base64.b64encode(contents).decode("utf-8")
    file_.close()
    child_html += f'<img src="data:image/jpg;base64,{data_url}" alt="test" style=margin:3px;width:100px;></img>\n'

gallery_html = opening_html
gallery_html += child_html
gallery_html += closing_html

labels_placeholder.empty()
labels_placeholder.markdown(gallery_html, unsafe_allow_html=True)

# file_ = open("../data/hand.jpg", "rb")
# contents = file_.read()
# data_url = base64.b64encode(contents).decode("utf-8")
# file_.close()

# st.markdown(
#     f'<img src="data:image/jpg;base64,{data_url}" alt="test" style=margin:3px;width:200px;>',
#     unsafe_allow_html=True,
# )